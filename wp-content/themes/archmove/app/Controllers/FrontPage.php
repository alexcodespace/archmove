<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public static function title()
    {
        return get_the_title();
    }

    public function content()
    {
        if (is_home()) {
            return get_the_content();
        }
        return get_the_title();
    }

//Hero section functionality
    public function hero_title()
    {
        return get_field('hero_title');
    }

    public function hero_descriprion()
    {
        return get_field('hero_description');
    }

    public function hero_image()
    {
        $image = get_field('hero_image');
        return $image;
    }

    public function hero_button()
    {
        return get_field('hero_button');
    }

//Projects section functionality
    public function projects_title()
    {
        return get_field('projects_title');
    }

    public function projects_slider()
    {
        $data = [];

        $projects = get_field('projects_slider');

        foreach ($projects as $p) {
            $pid = $p->ID;

            $this_post = (object)array(
                'thumbnail' => get_the_post_thumbnail($pid, 'project-slider', array('class' => 'card-img-top img-fluid')),
                'permalink' => get_the_permalink($pid),
                'title' => get_the_title($pid),
            );

            array_push($data, $this_post);
        }

        return $data;

    }

// Skills section functionality
    public function skills_title()
    {
        return get_field('skills_title');
    }

    public function skills_image()
    {
        return get_field('skills_image');
    }

    public function skills_items()
    {
        //$items = get_field('skills_items');
        $datas = [];
        if (have_rows('skills_items')):
            while (have_rows('skills_items')) : the_row();
                $sub_icon = get_sub_field('skills_item_icon');
                $sub_title = get_sub_field('skills_item_title');
                $sub_description = get_sub_field('skills_item_description');

                $this_post = (object)array(
                    'icon' => $sub_icon,
                    'title' => $sub_title,
                    'description' => $sub_description,
                );

                array_push($datas, $this_post);

            endwhile;
        endif;
        return $datas;
    }

// Testimonials section functionality
    public function testimonials_title()
    {
        return get_field('testimonials_title');
    }

    public function testimonials_slider()
    {
        $data_testimonial = [];

        $testimonials = get_field('testimonials_slider');

        foreach ($testimonials as $t) {
            $pid = $t->ID;
            $this_post = (object)array(
                'title' => get_the_title($pid),
                'testimonial_author' => get_field('testimonial_author', $pid),
                'testimonial_position' => get_field('testimonial_position', $pid),
                'testimonial_content' => get_the_content('', '', $pid),
            );

            array_push($data_testimonial, $this_post);
        }

        return $data_testimonial;
    }

//Promo section functionality
    public function promo_title()
    {
        return get_field('promo_title');
    }

    public function promo_image()
    {
        return get_field('promo_image');
    }

    public function promo_button()
    {
        return  get_field('promo_button');
    }

// Partners section functionality
    public function title_partners()
    {
        return get_field('title_partners');
    }

    public function partners()
    {
        $data_partners = [];

        if (have_rows('partners')):
            while (have_rows('partners')) : the_row();
                $sub_images = get_sub_field('partner_item_images');
                $sub_link = get_sub_field('partner_item_link');

                $this_post = (object)array(
                    'images' => $sub_images,
                    'link' => $sub_link,
                );

                array_push($data_partners, $this_post);

            endwhile;
        endif;

        return $data_partners;
    }
}

