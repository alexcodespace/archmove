<?php

add_action('init', 'projects_post_type');
function projects_post_type(){
    register_post_type('projects', array(
        'labels'             => array(
            'name'               => 'Projects', // Основное название типа записи
            'singular_name'      => 'Project', // отдельное название записи типа Book
            'add_new'            => 'Add new',
            'add_new_item'       => 'Add new project',
            'edit_item'          => 'Edit project',
            'new_item'           => 'New project',
            'view_item'          => 'View project',
            'search_items'       => 'Find project',
            'not_found'          => 'Project doesn\'t exist',
            'not_found_in_trash' => 'Project doesn\'t find in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Projects'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'show_in_rest'       => true,
        'hierarchical'       => false,
        'supports'           => array('title','editor','thumbnail')
    ) );
}

add_action('init', 'testimonials_post_type');
function testimonials_post_type()
{
    register_post_type('testimonial', array(
        'labels' => array(
            'name' => 'Testimonial', // Основное название типа записи
            'singular_name' => 'Testimonial', // отдельное название записи типа Book
            'add_new' => 'Add new',
            'add_new_item' => 'Add new testimonial',
            'edit_item' => 'Edit testimonial',
            'new_item' => 'New testimonial',
            'view_item' => 'View testimonial',
            'search_items' => 'Find testimonial',
            'not_found' => 'Testimonial doesn\'t exist',
            'not_found_in_trash' => 'Testimonial doesn\'t find in trash',
            'parent_item_colon' => '',
            'menu_name' => 'Testimonials'

        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'show_in_rest' => true,
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'thumbnail')
    ));
}
