<section class="arch-projects">

  <h2 class="arch-projects__title text-center">{!! $projects_title !!}</h2>

  <div id="arch-projects-slider" class="swiper-container">

    <div class="swiper-wrapper">
      @foreach( $projects_slider as $p)
        <div class="swiper-slide">
          {!! $p->thumbnail !!}
            {{ $p->title }}
        </div>
      @endforeach
    </div>

  </div>

</section>
