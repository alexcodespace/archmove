<section class="arch-promo">
  <div class="container">
    <div class="row">
      <div class="col-5">
        <img class="arch-promo__image" src="{!! $promo_image['url']; !!}}" alt="{!! $promo_image['alt']; !!}">
      </div>
      <div class="col-7">
        <h1 class="arch-promo__title text-center">{!! $promo_title; !!}</h1>
        <a class="arch-hero__buttton" href="{!! $promo_button['url'] !!}" target="{!! $promo_button['targer'] ? $promo_button['targer'] : '_self' !!}">{{ $promo_button['title'] }}</a>
      </div>

    </div>

  </div>
</section>
