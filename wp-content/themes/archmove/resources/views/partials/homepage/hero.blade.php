<section class="arch-hero">
  <div class="container">
    <h1 class="arch-hero__title">{!! $hero_title; !!}</h1>
    <div class="row">
      <div class="col-lg-4 col-sm-12">
        <div class="arch-hero__description">{!! $hero_descriprion !!}</div>

        <div class="arch-hero__button">
          <a class="arch-hero__button-link" href="{!! $hero_button['url'] !!}" target="{!! $hero_button['targer'] ? $hero_button['targer'] : '_self' !!}">{{ $hero_button['title'] }}</a>
        </div>
      </div>
      <div class="col-lg-8 col-sm-12">
        <img class="arch-hero__image" src="{!! $hero_image['url']; !!}}" alt="{!! $hero_image['alt']; !!}">
      </div>
    </div>

  </div>
</section>
