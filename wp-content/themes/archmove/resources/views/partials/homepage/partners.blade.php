<section class="partners-slider">
  <div class="container">
  <h2 class="text-center">{!! $title_partners !!}</h2>
<div class="row row-cols-5 align-items-center">
  @foreach( $partners as $partner)
    <div class="col">
    <a href="{{ $partner->link }}" target="_blank">
      <img src="{!! $partner->images['url']; !!}" alt="{!! $partner->images['alt']; !!}">
    </a>
    </div>
  @endforeach
</div>
  </div>
</section>
