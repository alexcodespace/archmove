<section class="skills-slider">
  <div class="container">
    <h2>{!! $skills_title !!}</h2>
    <div class="row">
      <div class="col-6">
        <img src="{!! $skills_image['url']; !!}}" alt="{!! $skills_image['alt']; !!}">
      </div>

      <div class="col-6">
        <div class="row">
        @foreach( $skills_items as $item)
          <div class="col-2">
            <img src="{!! $item->icon['url']; !!}}" alt="{!! $item->icon['alt']; !!}">
          </div>
        <div class="col-10">
          {{ $item->title }}
          {{ $item->description }}
        </div>
        @endforeach
        </div>
      </div>
    </div>

  </div>
</section>
