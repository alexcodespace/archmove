<section class="testimonials-slider">
  <div class="container">
  <h2 class="text-center">{{ $testimonials_title }}</h2>
  <div id="arch-testimonial-slider" class="slick-slider">
    @foreach( $testimonials_slider as $testimonial_item)
      <div class="slick-slide">
        <div class="slick-caption">
          <div class="row justify-content-end">
            <div class="col-8 pull-2">
              {{ $testimonial_item->testimonial_author }}
              {{ $testimonial_item->testimonial_position }}
              {!! $testimonial_item->testimonial_content !!}
            </div>
          </div>

        </div>
      </div>
    @endforeach
  </div>
  </div>
</section>
