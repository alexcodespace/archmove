{{--
  Template Name: Homepage Template
--}}

@extends('layouts.homepage')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.homepage.hero')
  @include('partials.homepage.slider-post')
  @include('partials.homepage.skills')
  @include('partials.homepage.testimonials')
  @include('partials.homepage.promo')
  @include('partials.homepage.partners')
  @include('partials.content-page')
  @endwhile
@endsection
