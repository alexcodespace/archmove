// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'
import 'slick-carousel/slick/slick'
import Swiper from 'swiper/bundle'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';


/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

jQuery(document).on('ready', () => {
  const swip = new Swiper('#arch-projects-slider',{
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
  swip;

  // $('#arch-projects-slider').slick({
  //   cssEase: 'ease',
  //   fade: false,  // Cause trouble if used slidesToShow: more than one
  //   arrows: false,
  //   dots: true,
  //   infinite: false,
  //   speed: 500,
  //   autoplay: true,
  //   autoplaySpeed: 3000,
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  // });
  //
  // $('#arch-testimonial-slider').slick({
  //   cssEase: 'ease',
  //   fade: false,  // Cause trouble if used slidesToShow: more than one
  //   arrows: true,
  //   dots: true,
  //   infinite: true,
  //   speed: 500,
  //   autoplay: true,
  //   autoplaySpeed: 3000,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  // });
});
